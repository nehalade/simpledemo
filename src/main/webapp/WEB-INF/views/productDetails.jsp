<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>



<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">



<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />



<title>spring-microservices1: Product Details</title>
</head>



<body>



	<div class="container">
		<div class="row">
			<nav class="navbar navbar-inverse">
				<div class="container-fluid">
					<div class="navbar-header"></div>
					<div>
						<ul class="nav navbar-nav navbar-right">
							<li><a href="http://www.pivotal.io"> <img alt="bank"
									title="Bank" height="120"
									src="<c:url value="images/download.jfif" />" />
							</a></li>
						</ul>
					</div>
				</div>
			</nav>
			<div style="text-align: right">
				[ <a href="<c:url value='/'/>">Home</a> | <a href="productList">Products</a>
				]
			</div>
			<%-- <div style="text-align: right">[ <a href="<c:url value='/accounts/'/>">Home</a>
| <a href="accountList">Accounts</a> ]</div> --%>
		</div>

		<div class="row">

			<h1>Product Details</h1>

			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-3">Product ID:</div>
					<div class="col-sm-9">${prod.id}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-3">Product Name:</div>
					<div class="col-sm-9">${prod.name}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-3">Product Brand:</div>
					<div class="col-sm-9">${prod.brand}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-3">Product Price:</div>
					<div class="col-sm-9">${prod.price}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-3">Product Quantity:</div>
					<div class="col-sm-9">${prod.quantity}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-3">Product expire-date:</div>
					<div class="col-sm-9">${prod.expdate}</div>
				</div>
				<br />
				<div class="row">
					<div class="col-sm-3">Product manufacture-date:</div>
					<div class="col-sm-9">${prod.mgfdate}</div>
				</div>
			</div>

		</div>



	</div>



</body>



</html>
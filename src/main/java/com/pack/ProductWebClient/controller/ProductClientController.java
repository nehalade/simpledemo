package com.pack.ProductWebClient.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.pack.ProductWebClient.model.Product;

@Controller
public class ProductClientController {

	@Autowired
	RestTemplate restTemplate;

	@GetMapping("/")
	public String homePage() {
		return "index";

	}

	@GetMapping("/productList")
	public String getProductList(Model m) {
		Product[] product = restTemplate.getForObject("http://PRODUCT-CONSUMER/products", Product[].class);
		List<Product> list = Arrays.asList(product);
		m.addAttribute("prodList", list);
		return "productList";
	}

	@RequestMapping("/productDetails")
	public String productDetails(@RequestParam("number") Integer prodId, Model m) {
		Product product = restTemplate.getForObject("http://PRODUCT-CONSUMER/products/{prodId}", Product.class, prodId);
		m.addAttribute("prod", product);
		return "productDetails";
	}

}
